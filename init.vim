filetype off

" Enable syntax hightlighting
syntax enable

" Show relative line numbers
set relativenumber
set number

" Indent when moving to the next line while writing code
set autoindent

" Expand tabs into spaces
set expandtab

" Tab equals to 4 spaces (can exist further modifications for each language)
set shiftwidth=4

" Show a visual line under the cursor's current line
set cursorline

" Show the matching part of the pair for [] {} and ()
let python_highlight_all=2

" Enable folding
set foldmethod=indent
set foldlevel=99

" Show the row and col of the cursor
set ruler

" Set maximun length of lines
set wrap
set textwidth=79
set linebreak

" Setting darkmode
set background=dark

" Plugins
call plug#begin('~/.config/nvim/plugged')

" Themes
Plug 'morhetz/gruvbox'

" IDE
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'

" AutoFilling
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" CHATGPT
Plug 'MunifTanjim/nui.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', {'tag':'0.1.0'}
Plug 'jackMort/ChatGPT.nvim'
call plug#end()

lua require('chatgpt').setup()

colorscheme gruvbox
let g:gruvbox_contrast_dark="hard"

nnoremap <SPACE> <Nop>
let mapleader=" "

nmap <leader>nt :NERDTreeFind<CR>

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" To enable filetype configurations
filetype plugin on
augroup language_settings
    autocmd FileType python source /home/jorge/.config/nvim/ftplugin/python.vim
    autocmd FileType html source /home/jorge/.config/nvim/ftplugin/html.vim
    autocmd FileType css source /home/jorge/.config/nvim/ftplugin/css.vim
augroup END

    

